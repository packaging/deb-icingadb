#!/bin/bash
set -exo pipefail

if [ -d archive ]; then
	pushd archive

	apt-ftparchive packages . > Packages

	sudo bash -exo pipefail <<EOF
		echo "deb [trusted=yes] file://\$(pwd)/ ./" >> /etc/apt/sources.list
		apt-get update
EOF

	popd
fi

sudo bash -exo pipefail <<EOF
	apt-get install --allow-unauthenticated -y icingadb
	icingadb --version
EOF
